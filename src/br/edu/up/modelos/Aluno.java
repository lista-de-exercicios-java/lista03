package br.edu.up.modelos;
public class Aluno{

    private String nome;
    private Double nota1;
    private Double nota2;
    private Double nota3;
    private Double soma;
    public Double media;

    public void setNome(){ 
        this.nome = Prompt.lerLinha("digite seu nome: ");
    }

    public void setNota(){ 
        this.nota1 = Prompt.lerDecimal("digite sua 1 nota: "); 
        this.nota2 = Prompt.lerDecimal("digite sua 2 nota: "); 
        this.nota3 = Prompt.lerDecimal("digite sua 3 nota: "); 
    }

    public void somar(){
        this.soma = this.nota1 + this.nota2 + this.nota3; 
    }

    public void media(){
        this.media = this.soma/3;
    }

    public void status(){
        Prompt.imprimir("seu nome e: " + this.nome);
        Prompt.imprimir("sua media e: " + this.media);
    }


    //exercicio08
    public void verificar(){
        if(this.media >= 7.0){
           Prompt.imprimir("voce foi aprovado.");
        }
        else if(this.media <= 5.0){
           Prompt.imprimir("voce reprovou.");
        }
        else{
            Prompt.imprimir("voce esta de recuperacao.");
        }
    }


    }

   