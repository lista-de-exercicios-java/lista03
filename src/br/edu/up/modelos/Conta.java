package br.edu.up.modelos;
public class Conta {

    private int cliente;
    private Double kwh;
    private double valorConta;
    private double valorKwh;


    public void setCliente(){
       this.cliente = Prompt.lerInteiro("Digite o tipo de cliente (1 para residencia, 2 para comercio, 3 para industria):");
    }
       
    public void setKwh(){
        this.kwh = Prompt.lerDecimal("Digite a quantidade de kWh consumidos:");
    }    
     
    public void pagarValor(){
        switch (this.cliente) {
            case 1:
                this.valorKwh = 0.60;
                break;
            case 2:
                this.valorKwh = 0.48;
                break;
            case 3:
                this.valorKwh = 1.29;
                break;
            default:
                Prompt.imprimir("Tipo de cliente invalido.");
                break;
        }
        
        this.valorConta = this.kwh * this.valorKwh;
        Prompt.imprimir("O valor da conta de luz e: R$" + this.valorConta);
    }

}
