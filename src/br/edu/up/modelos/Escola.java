package br.edu.up.modelos;
public class Escola {

    private String nome;
    private int matricola;
    private int hrAula;
    private int profNivel;
    private Double salario;
    private Double notaLaboratorio;
    private Double notaSemestral;
    private Double notaExameFinal;
    private int pesoLaboratorio = 2;
    private int pesoSemestral = 3;
    private int pesoExameFinal = 5;
    private char classificacao;
    private Double notaFinal;


    public Escola(Double notaLaboratorio, Double notaSemestral, Double notaExameFinal){
        this.notaLaboratorio = notaLaboratorio;
        this.notaSemestral = notaSemestral;
        this.notaExameFinal = notaExameFinal;
    }

    public Escola(Double notaLaboratorio, Double notaSemestral) {
        this.notaLaboratorio = notaLaboratorio;
        this.notaSemestral = notaSemestral;
    }

    public  Escola(){
     
    }

    public void setNome(String nome){
        this.nome = nome;
    }

    public void setMatricola(int matricola){
        this.matricola = matricola;
    }

    public void setHrAula(){
        this.hrAula = Prompt.lerInteiro("digite a quantidade de hrs de aula dadas esse mes: ");
    }

    public void setProfNivel(){
        this.profNivel = Prompt.lerInteiro("digite o seu nivel como professor: ");
    }

    public void verificarSalario(){
        if(profNivel == 1){
            salario = hrAula * 12.00;
        }
        else if(profNivel == 2){
            salario = hrAula * 17.00;
        }
        else if(profNivel == 3){
            salario = hrAula * 25.00;
        }
        else{
            Prompt.imprimir("o nivel de professor digitado nao existe.");
        }

        Prompt.imprimir("o seu salario mesal e: " + salario);
    }

   public void calcularNotaFinal(){
      this.notaFinal = (notaLaboratorio * pesoLaboratorio + notaSemestral * pesoSemestral + notaExameFinal * pesoExameFinal) / (pesoLaboratorio + pesoSemestral + pesoExameFinal);
 
      Prompt.imprimir("sua nota final e: " + this.notaFinal);
   }

   public void verClassificacao(){
      if(notaFinal >= 8 && notaFinal <= 10){
        classificacao = 'A';
      }

      else if(notaFinal >= 7 && notaFinal < 8){
        classificacao = 'B';
      }

      else if(notaFinal >= 6 && notaFinal < 7){
        classificacao = 'C';
      }

      else if(notaFinal >= 5 && notaFinal < 6){
        classificacao = 'D';
      }
      else if(notaFinal == 0 && notaFinal < 5){
        classificacao = 'R';
      }
   }

   public void status(){
    Prompt.imprimir("seu nome e:" + this.nome);
    Prompt.imprimir("sua matricola e: " + this.matricola);
    Prompt.imprimir("sua classificacao e: " + this.classificacao);
   }

}
