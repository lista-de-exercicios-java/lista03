package br.edu.up.modelos;
public class Empresa {

    private String nome;
    private char sexo;
    private int idade;
    private int qtde;
    private Double salarioMin;
    private Double salarioTotal;
    private Double reajuste;
    private Double salario;
    private Double salarioAume;
    private Double abono;

    public void setNome(){
        this.nome = Prompt.lerLinha("digite seu nome: ");
    }

    public void setIdade(){
        this.idade = Prompt.lerInteiro("digite sua idade: ");
    }

    public void setSexo(){
        this.sexo = Prompt.lerCaractere("digite seu sexo ( M / F): ");
    }
    
    public void reajustarSalario(){

        qtde = Prompt.lerInteiro("digite o numero de funcionarios que vao ter um reajuste salarial: ");
        for(int i = 0; i < qtde; i++){
          salarioMin = Prompt.lerDecimal("digite  quantos salarios minimos voce ganha: ");
          salario = 1.412 * salarioMin;

        if(salarioMin < 3){
            reajuste = salario * 0.5;
        }
        else if(salarioMin >= 3 && salarioMin <=10){
            reajuste = salario * 0.2;
        }
        else if(salarioMin > 10 && salarioMin <= 20){
            reajuste = salario * 0.15;
        }
        else{
            reajuste = salario * 0.1;
        }

        salarioTotal = salario + reajuste;
        Prompt.imprimir("voce teve um reajuste de " + reajuste );
        Prompt.imprimir("salario total: " + salarioTotal );
      }
    }

    //exercicio17s

    public void reajustarSalario2(){
          
        this.salario = Prompt.lerDecimal("Salário do funcionário: ");

        this.salarioMin = Prompt.lerDecimal("Valor do salário mínimo: ");

        reajuste =  0.1;
        salarioTotal = this.salario * (1 + this.reajuste);

        Prompt.imprimir("nome do funcionário: " + this.nome);
        Prompt.imprimir("Reajuste: " + (reajuste * 100) + "%");
        Prompt.imprimir("Novo salário: R$" + this.salarioTotal);

        this.salarioAume = this.salarioTotal - this.salario;
        Prompt.imprimir("\nAumento na folha de pagamento da empresa: R$" + this.salarioAume);

    }

    public void abonar(){
        this.salario = Prompt.lerDecimal("Salário do funcionário: ");

        if(this.sexo == 'M'){
            if(this.idade >= 30){
                this.abono = 100.00;
            }
            else{
                this.abono = 50.00;
            }
        }

        else if(this.sexo == 'F'){
            if(this.idade >= 30){
                this.abono = 200.00;
            }
            else{
                this.abono = 80.00;
            }
        }
    

    this.salarioTotal = this.salario - this.abono;

    Prompt.imprimir("seu nome e: " + this.nome);
    Prompt.imprimir("seu salario liquido e: " + this.salarioTotal);
    Prompt.imprimir("sua idade e: " + this.idade);
    Prompt.imprimir("seu sexo e: " + this.sexo);

   }
}
