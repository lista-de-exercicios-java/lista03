package br.edu.up.modelos;
public class Triangulo {
    private int lado1;
    private int lado2;
    private int lado3;


    public void setLado(){
        this.lado1 = Prompt.lerInteiro("Digite o comprimento do lado 1 do triangulo:");;
        this.lado2 = Prompt.lerInteiro("Digite o comprimento do lado 2 do triangulo:");;
        this.lado3 = Prompt.lerInteiro("Digite o comprimento do lado 3 do triangulo:");;
    }
    
     public boolean verificarTriangulo() {
       return (this.lado1 < this.lado2 + this.lado3) && (this.lado2 < this.lado1 + this.lado3) && (this.lado3 < this.lado1 + this.lado2);
    }
    
    public void vericarTipo(){
      if (verificarTriangulo()) {
          if (this.lado1 == this.lado2 && this.lado2 == this.lado3) {
              Prompt.imprimir("Triângulo Equilátero");
            } 
          else if (this.lado1 == this.lado2 || this.lado1 == this.lado3 || this.lado2 == this.lado3) {
              Prompt.imprimir("Triângulo Isósceles");
            }
           else {
              Prompt.imprimir("Triângulo Escaleno");
            }
      } 
      else {
          Prompt.imprimir("Os lados fornecidos não formam um triângulo.");
        }
    }
}



