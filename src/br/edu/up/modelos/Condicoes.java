package br.edu.up.modelos;
public class Condicoes {
    private String[] nome;
    private int qtde;
    private int[] numero;
    private int[] numero2;
    private char[] sexo;
    private int a;
    private int b;

    public void setQtde() {
        this.qtde = Prompt.lerInteiro("Digite o numero de vezes que deseja utilizar o programa: ");
    }

    public void verificarNumeros() {
        this.numero = new int[this.qtde];

        for (int i = 0; i < this.qtde; i++) {
            this.numero[i] = Prompt.lerInteiro("Digite um numero: ");

            if (this.numero[i] >= 10 && this.numero[i] <= 150) {
                Prompt.imprimir("O numero " + this.numero[i] + " está entre os numeros 10 e 150.");
            } else {
                Prompt.imprimir("O número " + this.numero[i] + " nao esta entre os numeros 10 e 150.");
            }
        }
    }

    public void verificarIdades() {
        this.numero2 = new int[this.qtde];

        for (int i = 0; i < this.qtde; i++) {
            this.numero2[i] = Prompt.lerInteiro("Digite sua idade: ");
            
            if (this.numero2[i] >= 18) {
                Prompt.imprimir("Voce e maior de idade.");
            } 
            else {
                Prompt.imprimir("Você e menor de idade.");
            }
        }
    }

    public void setNomes() {
        this.nome = new String[this.qtde];

        for (int i = 0; i < this.qtde; i++) {
            this.nome[i] = Prompt.lerLinha("Digite seu nome: ");
        }
    }

    public void verificarSexo() {
        this.sexo = new char[this.qtde];

        for (int i = 0; i < this.qtde; i++) {
            this.sexo[i] = Prompt.lerCaractere("Digite seu sexo (M ou F): ");

            if (this.sexo[i] == 'M') {
                System.out.println("Voce é homem " + this.nome[i]);
                this.a++;
            } else {
                System.out.println("Voce é mulher " + this.nome[i]);
                this.b++;
            }
        }

        System.out.println("Existem " + this.a++ + " homens.");
        System.out.println("Existem " + this.b++ + " mulheres.");
    }
}

