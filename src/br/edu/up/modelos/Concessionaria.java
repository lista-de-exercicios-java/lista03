package br.edu.up.modelos;
public class Concessionaria {

    private String resposta;
    private String tipoCarro;
    private int anoCarro;
    private Double valorCarro;
    private Double descontoCarro;
    private Double somaCarro;
    private Double totalCarro= 0.0;


    public void desconto(){
        resposta = "sim";

        do {
        
        anoCarro = Prompt.lerInteiro("digite o ano do carro: ");
        valorCarro = Prompt.lerDecimal("digite o valor do carro: ");

        if(anoCarro <= 2000){
           descontoCarro = valorCarro * 0.12;

        }
        else{
            descontoCarro = valorCarro * 0.07;
        }
 
        somaCarro = valorCarro - descontoCarro;
        totalCarro += somaCarro; 

        Prompt.imprimir("o total das compras e: " + totalCarro);
        resposta = Prompt.lerLinha("voce quer continuar comprando ?:");

      } while(resposta.equalsIgnoreCase("Sim"));
    } 

    //exercicio15
    public void desconto2 (){

        valorCarro = Prompt.lerDecimal("digite o valor do carro: ");
        tipoCarro = Prompt.lerLinha("digite o tipo de combustivrl do carro (gasolina / alcool / diesel): ");
      
        if (tipoCarro.equals("gasolina")) {
            descontoCarro = valorCarro * 0.21;
        } 
        else if (tipoCarro.equals("alcool")) {
            descontoCarro = valorCarro * 0.25;
        } 
        else if (tipoCarro.equals("diesel")) {
            descontoCarro = valorCarro * 0.14;
        }
        else {
            System.out.println("Tipo de combustivel invalido.");
            return; 
        }

        totalCarro = valorCarro - descontoCarro; 

        Prompt.imprimir("o valor do desconto e: " + descontoCarro);
        Prompt.imprimir("o valor total do carro e: " + totalCarro);
        
    }
}