package br.edu.up.modelos;
public class Piscina {

    private int idade;

    public void setIdade(){
       this.idade = Prompt.lerInteiro("Digite a idade do nadador:");
    }
    
    public void verCategoria(){
      if (this.idade >= 5 && this.idade <= 7) {
          Prompt.imprimir("Categoria: Infantil A");
      } 
      else if (this.idade >= 8 && this.idade <= 10) {
          Prompt.imprimir("Categoria: Infantil B");
      } 
      else if (this.idade >= 11 && this.idade <= 13) {
          Prompt.imprimir("Categoria: Juvenil A");
      } 
      else if (this.idade >= 14 && this.idade <= 17) {
          Prompt.imprimir("Categoria: Juvenil B");
      } 
      else if (this.idade >= 18 && this.idade <= 25) {
          Prompt.imprimir("Categoria: Sênior");
      } 
      else {
          Prompt.imprimir("Idade fora da faixa etária");
      }
    }
}

