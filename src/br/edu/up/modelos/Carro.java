package br.edu.up.modelos;
public class Carro {

    private Double disPercorida;
    private Double litrosTotal;
    private Double conMedio;
    
    public void setDisPercorida(){
        this.disPercorida = Prompt.lerDecimal("digite o total de litros gasto: ");
    }
 
    public void setLitrosTotal(){
        litrosTotal = Prompt.lerDecimal("digite o total de litros gasto: ");
    }

    public void  conMedio(){           
        this.conMedio = disPercorida / litrosTotal;  
    }

    public void status(){
        Prompt.imprimir("o consumo medio do carro e :" + this.conMedio);
    }
    
}
