package br.edu.up.modelos;
public class Banco {

    private Double real;
    private Double dolarValor;
    private Double converssao;

    public void setReal(){
      this.real = Prompt.lerDecimal("digite a quantidade de real(R$) que vc quer converter: ");
    }

    public void setDolarValor(){
      this.dolarValor = Prompt.lerDecimal("digite o valor(cotacao) do dolar: ");
    }

    public void converter(){
      this.converssao = this.real / this.dolarValor;
    }

    public void status(){
      Prompt.imprimir("a conversao de real para dolar e: " + this.converssao);
    }
}