package br.edu.up.modelos;

public class Montadoura {
    private Double valorCarro;
    private Double somaImposto;
    private Double somaDistribuidora;
    private Double total;

    public void setValorCarro(Double valorCarro) {
        this.valorCarro = valorCarro;
    }

    public void SomarImposto(){
        this.somaImposto= valorCarro * 0.45;
    }

    public void SomarDistribuidora(){
        this.somaDistribuidora = somaImposto * 0.28;
    }

    public void verTotal(){
       this.total = somaImposto + valorCarro + somaDistribuidora;
    }

    public void status(){
        Prompt.imprimir("o valor total do carro e: " + total);
    }
}
