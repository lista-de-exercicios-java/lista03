package br.edu.up.modelos;
public class Loja{
    private String nome;
    private Double salario;
    private Double vendasTotal;
    private Double vendas;
    private Double salarioTotal;
    private Double comissao; 
    private Double parcelar;
    private Double precoCusto;
    private Double precoVenda;
    private Double somaCusto = 0.0;
    private Double somaVenda = 0.0;
    private Double mediaCusto;
    private Double mediaVenda;
    private Double lucro;
    private Double acrescimo;

    public Loja() {
    }

    public Loja(Double vendas, Double acrescimo) {
        this.vendas = vendas;
        this.acrescimo = acrescimo;
    }

    public void somar(){
        this.vendasTotal = this.vendas + this.acrescimo;
        Prompt.imprimir("o valor de venda do produto e: " + this.vendasTotal);
    }

    public void setNome(){
        this.nome = Prompt.lerLinha("digite seu nome: ");
    }
    
    public void setSalario(){
        this.salario = Prompt.lerDecimal("digite seu salario fixo: ");
    }

    public void setVendasTotal(){
        this.vendasTotal = Prompt.lerDecimal("digite o seu total de vendas: ");
    }

    public void setVendas(){
        this.vendas = Prompt.lerDecimal("digite o valor da sua compra: ");
    }

    public void setComissao(){
        this.comissao = this.vendasTotal * 0.15;
    }

    public void setSalarioTotal(){
        this.salarioTotal = comissao + this.salario;
    }

    public void status(){
        Prompt.lerLinha("seu nome e: " + this.nome);
        Prompt.lerLinha("sua comissao no total de vendas e: " + comissao);
        Prompt.lerLinha("seu salario total e: " + salarioTotal);
    }

    public void parcelar(){
        this.parcelar = vendas / 5;
    }

    public void status2(){
        Prompt.lerLinha("seu nome e: " + this.nome);
        Prompt.imprimir("o valor compra parcelando em 5 vezes e: " + this.parcelar);
    }


    public void lucrar(){
        for(int i = 0; i < 40; i++){
           precoCusto = Prompt.lerDecimal("digite o valor de custo do produto: ");
           precoVenda = Prompt.lerDecimal("digite o valor de venda do produto: ");

           lucro = precoVenda - precoCusto;

           somaCusto += precoCusto;
           somaVenda += precoVenda;

           if (precoVenda > precoCusto) {
            Prompt.imprimir("Lucro para o produto " + (i + 1) + " e: " + lucro);
           }
           else if (precoVenda < precoCusto) {
            Prompt.imprimir("Prejuízo para o produto "  + (i + 1) + " e: " + lucro);
           }
           else {
            Prompt.imprimir("sem lucro para o produto " + (i + 1));
           }
        }
    }

    public void verMediaProduto(){
        mediaCusto = somaCusto / 40;
        mediaVenda = somaVenda / 40;

        Prompt.imprimir("Média de preço de custo: " + mediaCusto);
        Prompt.imprimir("Média de preço de venda: " + mediaVenda);
    }
    
}