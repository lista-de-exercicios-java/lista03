package br.edu.up.modelos;
public class Balanca {

    private String nome;
    private char sexo;
    private Double altura;
    private int idade;
    private Double pesoIdeal;

    public Balanca(String nome, char sexo, Double altura, int idade){
        this.nome = nome;
        this.sexo = sexo;
        this.altura = altura;
        this.idade = idade;
    }

    public Balanca(){

    }

    public void calcularPesoIdeal(){
        if(this.sexo == 'M'){

            if(this.altura > 1.70){

                if(this.idade <= 20){
                    pesoIdeal =  (72.7 * this.altura) - 58;
                }
                else if(this.idade >= 21 && this.idade <=39){
                    pesoIdeal =  (72.7*this.altura) - 53;
                }
                else if(this.idade >= 40){
                    pesoIdeal =  (72.7*this.altura) - 45;
                }
            }
            else if(this.altura <= 1.70){

                if(this.idade <= 40){
                    pesoIdeal =  (72.7*this.altura) - 50;
                }
                else if(this.idade > 40){
                    pesoIdeal =  (72.7*this.altura) - 58;
                }
            }
        }

        else if(this.sexo == 'F'){

            if(this.altura > 1.50){
                pesoIdeal =  (62.1*this.altura) - 44.7;
            }

            else if(this.altura <= 1.50){

                if(this.idade >= 35){
                    pesoIdeal =  (62.1*this.altura) - 45;
                }
                else if(this.idade < 35){
                    pesoIdeal =  (62.1*this.altura) - 49;
                }
            }
        }
    }

    public void status(){
        Prompt.imprimir("seu nome e: " + this.nome);
        Prompt.imprimir("seu pesso ideal e: " + this.pesoIdeal);
    }

}
