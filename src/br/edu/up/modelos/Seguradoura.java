package br.edu.up.modelos;
public class Seguradoura {

    private String nome;
    private int idade;
    private int categoria;
    private char grupoRisco;

    public void setNome(){
        this.nome = Prompt.lerLinha("digite seu nome: ");
    }

    public void setIdade(){
        this.idade = Prompt.lerInteiro("digite sua idade: ");
    }

    public void setGrupoRisco(){
        this.grupoRisco = Prompt.lerCaractere("digite sua categoria de risco B(baixa) , M(medio), A(alta): ");
    }

    public void verCategoria(){
        if(this.idade >= 17 && this.idade <= 20){
            if(this.grupoRisco == 'B'){
                this.categoria = 1;
            }
            else if(this.grupoRisco == 'M'){
                this.categoria = 2;
            }
            else if(this.grupoRisco == 'A'){
                this.categoria = 3;
            }
        }
        else if(this.idade >= 21 && this.idade <= 24){
            if(this.grupoRisco == 'B'){
                this.categoria = 2;
            }
            else if(this.grupoRisco == 'M'){
                this.categoria = 3;
            }
            else if(this.grupoRisco == 'A'){
                this.categoria = 4;
            }
        }
        else if(this.idade >= 25 && this.idade <= 34){
            if(this.grupoRisco == 'B'){
                this.categoria = 3;
            }
            else if(this.grupoRisco == 'M'){
                this.categoria = 4;
            }
            else if(this.grupoRisco == 'A'){
                this.categoria = 5;
            }
        }
        else if(this.idade >= 35 && this.idade <= 64){
            if(this.grupoRisco == 'B'){
                this.categoria = 4;
            }
            else if(this.grupoRisco == 'M'){
                this.categoria = 5;
            }
            else if(this.grupoRisco == 'A'){
                this.categoria = 6;
            }
        }
        else if(this.idade >= 65 && this.idade <= 70){
            if(this.grupoRisco == 'B'){
                this.categoria = 7;
            }
            else if(this.grupoRisco == 'M'){
                this.categoria = 8;
            }
            else if(this.grupoRisco == 'A'){
                this.categoria = 9;
            }
        }
    }

    public void status(){
        Prompt.imprimir("seu nome e: " + this.nome);
        Prompt.imprimir("sua idade e: " + this.idade);
        Prompt.imprimir("seu grupo de risco e: " + this.grupoRisco );
        Prompt.imprimir("sua categoria e: " + this.categoria);

        if(this.idade < 17 || this.idade > 70){
            Prompt.imprimir("voce nao esta dentro da idade permitida.");
        }
    }

}
